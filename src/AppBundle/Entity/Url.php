<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpKernel\Kernel as SymfonyKernel;

/**
 * @IgnoreAnnotation("fn")
 */
class Url
{
    /**
     * @var int
     */
    private $uId;

    /**
     * @var string
     */
    private $uOriginalUrl;

    /**
     * @var string
     */
    private $uHash;

    /**
     * @var \DateTime
     */
    private $uCreated;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->uId;
    }

    /**
     * Set originalUrl
     *
     * @param string $originalUrl
     *
     * @return Url
     */
    public function setOriginalUrl($originalUrl)
    {
        $this->uOriginalUrl = $originalUrl;

        return $this;
    }

    /**
     * Get originalUrl
     *
     * @return string
     */
    public function getOriginalUrl()
    {
        return $this->uOriginalUrl;
    }

    /**
     * Set hash
     *
     * @return Url
     */
    public function setHash()
    {
        $this->uHash = sprintf('%x', crc32($this->uOriginalUrl . SymfonyKernel::VERSION));

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->uHash;
    }

    /**
     * Set created
     *
     * @return Url
     */
    public function setCreated()
    {
        $this->uCreated = new \DateTime("now");

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->uCreated;
    }
}


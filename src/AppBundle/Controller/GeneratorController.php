<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Url;

/**
 * @Route(name="generator_")
 */ 
class GeneratorController extends Controller
{
    /**
     * @Route("/generator", name="index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('generator/index.html.twig');
    }

    /**
     * @Route("/generator/process", name="process")
     */
    public function processAction(Request $request)
    {
        $originalUrl = $request->request->get('original');

        $repository = $this->getDoctrine()->getRepository(Url::class);

        if (!$url = $repository->findOneBy(['uOriginalUrl' => $originalUrl])) {

            $entityManager = $this->getDoctrine()->getManager();

            $url = (new Url())
                ->setOriginalUrl($originalUrl)
                ->setHash()
                ->setCreated();

            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($url);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();
        }

        return new Response($request->getSchemeAndHttpHost() . '/' . $url->getHash());
    }
}

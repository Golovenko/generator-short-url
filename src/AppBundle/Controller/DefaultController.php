<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Url;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/{hash}", name="app_redirect")
     */
    public function redirectAction($hash)
    {
        $repository = $this->getDoctrine()->getRepository(Url::class);

        $url = $repository->findOneBy(['uHash' => $hash]);

        return !$url ? $this->redirectToRoute('homepage') : $this->redirect($url->getOriginalUrl());
    }
}
